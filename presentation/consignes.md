
Votre soutenance doit se faire avec des transparents (Beamer, PowerPoint, Keynote, ...). Cette année, les soutenances auront lieu les 4 et 5 septembre 2023 au DI devant un jury composé de membres du LIP. 

La soutenance est planifiée sur 30 minutes : 17' présentation + 10' questions (et 3 minutes de marge). La langue de la soutenance est a priori le français (mais elle peut se faire en anglais selon votre souhait, de même pour les transparents), sauf disposition particulière décidée avec le département de langues.

Il est fortement conseillé de préparer votre soutenance avec votre encadrant(e) et de faire au moins une répétition avec lui/elle et/ou l'équipe d'accueil (voir la Charte des stages). Un regard extérieur est indispensable pour tester vos choix et calibrer la présentation (clarté, timing, choix de présenter tel résultat plutôt que tel autre, ...), et votre présentation bénéficiera énormément de l'expérience de personnes ayant l'habitude de présenter leurs travaux.

Je vous invite à numéroter clairement chaque slide (e.g., 1/23 en bas à droite) afin que nous puissions déterminer où vous en êtes dans votre présentation. Également, je vous invite à répéter votre présentation : nous serons intransigeants sur la durée de votre exposé. Si vous n'avez pas fini au bout du temps imparti (17 minutes), vous serez interrompu(e) et ceci sera retenu comme une erreur de préparation. Encore une fois, ces contraintes sont de rigueur lorsque l'on présente des travaux dans toute conférence. De même que pour les contraintes de pages pour le rapport, il est tout à fait possible de faire une excellente présentation de votre travail dans le temps imparti, et faire ce travail de synthèse est aussi une bonne façon de montrer que vous maîtrisez bien ce que vous avez étudié.

La soutenance doit expliquer votre sujet de stage, les questions que vous avez étudiées (dans quel cadre cela s'inscrit, pourquoi c'est intéressant, quelles sont les connaissances actuelles sur le sujet, ...) puis le travail que vous avez mené afin de résoudre le problème. 

Planning :

Le planning n'est pas encore défini, le jury n'étant pas complet. Cependant, je peux déjà vous dire que les horaires de passage ne sont pas librement échangeables, les membres du jury étant choisis en fonction des exposés. Il est possible d'échanger au sein d'une même session (en avertissant à l'avance), mais pas de changer de session. Les soutenances devraient se tenir le lundi 4 de 9h à 17h et le mardi 5 de 9h à 12h environ. 

Matériel :

Il est d'usage que vous ameniez votre propre ordinateur pour faire votre présentation (connexion en hdmi ou vga au vidéo-projecteur). Vous pouvez bien sûr vous arranger entre vous, mais il n'y aura pas de machine disponible sur place.
