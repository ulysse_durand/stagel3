\newcommand{\letitle}{Stage de L3, formalisation de Game Semantics avec Coq}
\newcommand{\leauthor}{Ulysse Durand\\\\\\dirigé par \\\\Pierre Clairambault\\ et \\Etienne Miquey\\\\entre le LIS et l'I2M à Luminy, Marseille}

\documentclass[a4paper,12pt]{article}
\usepackage{tikzit}

\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage[style=numeric,backend=biber]{biblatex}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{tikzit}
\usepackage{bbold}
\usepackage[xcolor,leftbars]{changebar}
\usepackage{framed}
\usepackage{ulem}
\usepackage{fontawesome5}

\setcounter{secnumdepth}{1}
\newenvironment{myindentpar}
 {\begin{list}{}
         {\setlength{\leftmargin}{1em}}
         \item[]
 }
 { \end{list}}

\renewenvironment{leftbar}{
  \def\FrameCommand{\vrule width 1pt \hspace{10pt}}
  \MakeFramed {\advance\hsize-\width \FrameRestore}
\setlength{\parskip}{10pt}}
 {\endMakeFramed}



\definecolor{DarkBlue}{RGB}{0,16,80}
\newcommand{\norm}[1]{\lvert #1 \rvert}

\newenvironment{answer}[1]
{
  \vspace{0.5cm} \noindent \begin{minipage}{\linewidth}
    \textbf{\textit{\uline{#1}}}
    \begin{leftbar}
}
{
    \end{leftbar}\end{minipage}\vspace{0.2cm}
}

\newenvironment{answernopage}[1]
{
  \vspace{0.5cm} \noindent
  \textbf{\textit{\uline{#1}}}
  \begin{leftbar}
}
{
    \end{leftbar}\vspace{0.2cm}
}

\usepackage{listings}
\lstset
{
    language=Caml,
    basicstyle=\footnotesize,
    numbers=left,
    stepnumber=1,
    showstringspaces=false,
    tabsize=1,
    breaklines=true,
    breakatwhitespace=false,
}


\usepackage{hyperref}
\hypersetup{
    colorlinks=truem,
    linkcolor=black,
    filecolor=red,
    urlcolor=blue
}

\urlstyle{same}

\addbibresource{bib.bib}


\everymath{\displaystyle}
\title{\letitle}
\author{\leauthor}
\date{}
\begin{document}
\maketitle

\newpage

\section{Introduction}

La sémantique de jeux est un domaine étudié en logique, plus précisément en
théorie de la programmation.
En effet, on peut avec elle étudier et raisonner sur l'exécution d'un programme
tout comme sur la véracité d'une proposition logique.
Pour raisonner sur une proposition logique, on imagine un jeu où l'opposant
cherche à montrer que la proposition n'est pas une tautologie alors que le joueur
cherche à montrer que c'est une tautologie. Par exemple, pour la proposition
(A et (B ou (non B)) opposant commence en disant "je pense que tu ne peux montrer
(B ou (non B))", le joueur rétorque "je pense que je peux montrer (non B)". Ainsi
une stratégie gagnante dans ce jeu est une preuve que la proposition est une
tautologie.
Pour ce qui est des programmes, l'execution du programme revient à une partie.
Pour executer 7+x, par exemple, l'opposant demande "que vaut 7+x : int",
le joueur répond "que vaut x : int ?", l'opposant répond 3, le joueur répond 10.
L'opposant a le rôle d'interface avec l'environnement.

Pour la sémantique des jeux il y a différents formalismes, avec des spécificités
différentes, et les constructions permettant d'obtenir une catégorie ne sont
souvent qu'esquissées car souffrent d'une technicité rendant difficile
la formalisation dans un assistant de preuves.
Dans un article dont Pierre Clairambault est un auteur, il est proposé une
formalisation qui semble unifiante et implémentable dans un assistant de preuve,
fournissant ainsi un cadre pour raisonner en Coq sur les jeux et stratégies.
\cite{game}.

Le but de ce stage est de remanier les définitions plutôt ensemblistes de cette
formalisation en des définitions inductives et d'en profiter pour faire des
preuves en Coq.

L'objectif à long terme de ce travail serait d'aller jusqu'à montrer qu'on a bien
une catégorie cartésienne fermée des jeux et stratégies. Mon apport dans ce stage
est de montrer que la composition de deux stratégies est bien une
stratégie.
Concrètement, j'ai été ammené dans mon stage à assimiler les notions de sémantique
des jeux et la formalisation de Pierre Clairambault, et prolonger l'esquisse de
formalisation qu'Étienne Miquey a proposé en Coq.

Nous allons alors voir dans un premier temps quelles sont ces définitions
plutôt ensemblistes présentées dans la formalisation, puis dans un second temps
quelles définitions inductives nous aurons adopté dans Coq, et quelles preuves
nous avons pu faire.
Le document comporte une annexe avec quelques preuves non formalisées en Coq.

\newpage
\section{Définitions usuelles de sémantique de jeu}

Afin d'obtenir, d'un point de vue algébrique, une catégorie des jeux et
stratégies, nous allons définir petit à petit les jeux et stratégies.
Commençons par définir ce qui constituera notre structure de jeu,
les structures d'événement.

Dans un jeu nous avons des événements, et nous verrons par la suite qu'une
partie est entre autres une suite finie d'événements.
Un événement dépend d'autres événements : dans une partie, pour qu'un événement
ait lieu, il faut que ceux dont il dépend aient déjà eu lieu.
Il y a donc une relation de causalité entre les événements du jeu qui est
un ordre partiel (si A dépend de B qui dépend de C alors A dépend de C).
Pour qu'un événement soit atteignable dans une partie on va exiger qu'il
dépende d'un nombre fini d'événements.

De plus certains événements sont incompatibles, deux événements incompatibles
ne peuvent arriver tous deux dans une partie, on dira qu'ils sont en conflit.
Enfin si deux événements sont en conflit,
alors les événements qui dépendent d'eux sont aussi en conflit.



\begin{answer}{Structure d'événement (S.E.) :}

Il s'agit de $E = (|E|, \leq_E, \#_E)$ où
\begin{itemize}
\item $\leq_E$ (la causalité) est un ordre partiel sur $E$
\item $\#_E$ (le conflit) est une relation binaire symétrique irréflexive :
\begin{itemize}
\item à causes finies :

\begin{equation*}\forall e \in |E|, \{e' \in |E| \mid e' \leq_E e\}\text{ est fini}\end{equation*}

\item qui vérifie l'axiome de vendetta :

\begin{equation*}\forall e_1 \#_E e_2, (e_2 \leq_E e'_2 \implies e_1 \# e'_2)\end{equation*}
\end{itemize}
\end{itemize}
\end{answer}

Maintenant, une configuration est un état possible du jeu.
On veut alors que ce soit un ensemble d'événements deux à deux non en conflit
tel que chaque événement a tous les événements dont il dépend dans la
configuration.

\begin{answer}{Configuration :}

On dit que $x$ fini $\subset |E|$ est une configuration si
\begin{itemize}
\item $x$ est fermé vers le bas :

\begin{equation*}\forall e \in x, \forall e' \in |E|, e' \leq_E e \implies e' \in x\end{equation*}

\item $x$ est sans conflit

\begin{equation*}\forall e, e' \in x, \neg (e \#_E e')\end{equation*}
\end{itemize}
\end{answer}

On note $\mathcal{C}(E)$ l'ensemble des configurations de $E$.

\begin{answer}{Justification :}
On dit que $x \in \mathcal{C}(E)$ justifie $e \in |E|$, noté $x \vdash e$ si
$x \uplus \{e\} \in \mathcal{C}(E)$
\end{answer}

Les jeux n'auront que deux joueurs, un appelé opposant (celui qui commence)
et l'autre est le joueur, on leur attribue une polarité (- et +).

Un jeu est alors une structure d'événement avec une polarité à chaque événement.
On précise en fait pour chaque événement si il est déclancheable par l'opposant
(polarité -) ou par le joueur (polarité +).

\begin{answer}{Jeu :}
Un jeu est $A$ une S.E munie d'une fonction

$\text{pol}_A : |A| \rightarrow \{-, +\}$
\end{answer}

Maintenant, une partie dans un jeu est une suite finie d'événements (on peut
dire que ce sont les coups) qui commence par l'opposant et qui alterne
(chacun son tour).

On impose aussi qu'après chaque coup on ait une configuration non déjà vue
(on ne peut pas passer notre tour).

\begin{answer}{Partie :}
Une partie sur $A$ est un mot $s = s_1 \dots s_n \in |A|^*$ qui est :
\begin{itemize}
\item valide :
$\forall 1\leq i \leq n, \{s_1, \dots s_i\} \in \mathcal{C}(A)$

\item non répétitive :
$\forall i, j, s_i = s_j \implies i = j$

\item alternante :
$\forall 1 \leq i \leq n-1, \text{pol}_A(s_i) = - \text{pol}_A(s_{i+1})$

\item négative :
$s \neq \epsilon \implies \text{pol}_A(s_1) = -$
\end{itemize}
\end{answer}

On note $\mathcal{P}(A)$ l'ensemble des parties sur $A$.

\subsubsection{Opérations sur les jeux}

À partir de jeux, il nous est possible d'en construire d'autres.
Nous allons voir qu'on peut construire un jeu où on joue à deux jeux
en parallèle ou bien un jeu où on inverse joueur et opposant.
\\

Dans le jeu où les jeux $A$ et $B$ sont mis en parallèle, chaque joueur décide
à chaque tour de jouer soit sur le premier jeu, soit sur le deuxième, tout en
respectant bien sûr la polarité des événements.

\begin{answer}{Jeu tenseur ou jeux parallèles :}
On définit le jeu $A_1 \otimes A_2$
\begin{itemize}
\item $|A_1 \otimes A_2| = \{1\} \times |A_1| \cup \{2\} \times |A_2|$
\item $(i,a) \leq_{A_1 \otimes A_2} (j, a') \iff i = j $ et $a \leq_{A_i} a'$
\item $(i,a) \#_{A_1 \otimes A_2} (j,a') \iff i = j$ et $a \#_{A_i} a'$
\item $\text{pol}_{A_1 \otimes A_2}(i,a) = pol_{A_i} (a)$
\end{itemize}
\end{answer}

Pour le jeu dual, on inverse simplement la polarité des événements. Les coups
opposants sont maintenant des coups joueur et les coups joueur sont maintenant
des coups opposant.

\begin{answer}{Jeu dual :}
On définit le jeu $A^\bot$
\begin{itemize}
\item $|A^\bot| = |A|$
\item $\leq_{A^\bot} = \leq_A$
\item $\#_{A^\bot} = \#_A$
\item $\text{pol}_{A^\bot} = - \text{pol}_A$
\end{itemize}
\end{answer}

\begin{answer}{Le jeu $A \vdash B$ :}
$A \vdash B = A^\bot \otimes B$
\end{answer}

On verra que cette définition est surtout utile pour définir des stratégies
avec leur composition.

\subsubsection{Stratégies sur les jeux}

Ici les stratégies que nous verrons sont des stratégies pour le joueur et non
l'opposant, moralement, personne n'a le contrôle sur ce que fait l'opposant.

Les stratégies pourraient être présentées comme des fonctions ou algorithmes
associant à une partie un événement à jouer, mais ici, nous les verrons comme
un ensemble de parties dans lequel on tenterait de rester lorsqu'on veut suivre
la stratégie.

On exige alors que lorsque le joueur cherche à rester dans cet ensemble,
au plus une partie soit présente pour poursuivre la partie en cours.
Il n'y aura pas toujours une partie à suivre mais si il y en a une c'est qu'il
y en a toujours eu jusque là.

\begin{answer}{Stratégie :}
Une stratégie sur le jeu $A$ est $\sigma \subset \mathcal{P}(A)$ qui est
\begin{itemize}
\item non vide :

$\epsilon \in \sigma$

\item clos par préfixe pair :

$\forall e \in \sigma, \forall e', |e'| \text{ pair } \implies e' \sqsubseteq e \implies e' \in \sigma$

\item déterministe :

(Dans une partie quand c'est le tour du joueur +, son choix de prochain coup
est unique, déterminé)

$sa_1^+, sa_2^+ \in \sigma \implies a_1 = a_2$

\end{itemize}
\end{answer}

Définissons les restrictions sur les parties d'un jeu $A_1 \otimes A_2$.

\begin{answer}{Restrictions sur les parties :}
Soit $a = a_1 a_2 \dots a_n$ une partie sur $A_1 \otimes A_2$,
on note $a \upharpoonright A_k$ la partie $(a_i)_{i \in I}$ où
$I = \{i / \exists b / a_i = (b,k)\}$
\end{answer}

Comme il n'y a pas de relation de causalité entre $A_1$ et $A_2$ dans
$A_1 \otimes A_2$, restreindre la partie sur $A_1 \otimes A_2$ à un côté
revient à ignorer ce qu'il se passe de l'autre.

\begin{answer}{Restriction de stratégie :}
Soit $s$ une stratégie sur $A_1 \otimes A_2$, alors $s\upharpoonright A_k$ est la
restriction de $\sigma$ à $A_k$.

$\sigma \upharpoonright A_k = \{a\upharpoonright A_k \mid a \in \sigma\}$

\end{answer}

\subsubsection{Catégories :}

Comme notre objectif reste de définir une catégorie des jeux et stratégies, nous
allons d'abord voir ce qu'est une catégorie puis comment on construit celle des
jeux et stratégies.

Dans la suite, ce qu'on appelle "classe" peut en fait être considéré comme
ensemble même si on définit ainsi que les petites catégories.

\begin{answer}{Catégorie :}
Une catégorie $\mathcal{C}$ est la donnée de
\begin{itemize}
\item Une classe d'objets $|\mathcal{C}|$

\item Pour chaque paire d'objets $A, B \in |\mathcal{C}|$, une classe de
morphismes $\mathcal{C}(A,B)$

\item Pour chaque $A \in |\mathcal{C}|$, un morphisme
$\text{id}_A \in \mathcal{C}(A,A)$

\item Une loi binaire sur les morphismes
$f \in \mathcal{C}(A,B)$, si $g \in \mathcal{C}(B,C)$, alors
$g \circ f \in \mathcal{C}(A,C)$
telle que
\begin{itemize}
\item $\circ$ est associative:
$h \circ (g \circ f) = (h \circ g) \circ f$

\item Pour tout A, $\text{id}_A$ est neutre pour $\circ$ :
$f \circ \text{id}_A = \text{id}_A \circ f = f$
\end{itemize}
\end{itemize}

\end{answer}

\subsection{La catégorie des jeux et des stratégies}

Nous allons tenter de construire la catégorie des jeux et stratégies

\begin{itemize}
\item Les objets seront les jeux
\item Les morphismes $\mathcal{C}(A,B)$ seront les stratégies sur le jeu $A \vdash B$
\item Pour A un objet, $\text{id}_A$ sera copycat de $A$ (défini très bientôt)
\item La composition de stratégies est définie très bientôt
\end{itemize}

Après avoir défini $\text{id}_A$ et $\circ$, il nous restera à prouver
que avec ces définitions, $\circ$ est associative et $\text{id}_A$ est neutre
pour $\circ$.

\subsubsection{Copycat}

Copycat de $A$ est une stratégie sur $A \vdash A = A^\bot \otimes A$
L'idée de la stratégie copycat est la suivante.

Si l'opposant joue un coup sur le jeu de gauche (resp. droite), alors on répond
en jouant ce même coup sur le jeu à droite (resp. gauche). Ceci est possible car
la polarité est opposée entre le jeu de gauche et droite,
un coup opposant d'un côté a un coup mirroir joueur sur le jeu de l'autre côté.

\begin{answer}{Definition :}
Copycat de $A$ est la stratégie définie par

$cc_A=\{s \in \mathcal{P}(A_1 \vdash A_2) \mid \forall t \sqsubseteq^+ s, t \upharpoonright A_1 = t \upharpoonright A_2\}$
\end{answer}


\subsubsection{Composition de stratégies}

Soit $\sigma$ stratégie sur $A\vdash B$ et $\tau$ stratégie sur $B \vdash C$,
construisons $\tau \circ \sigma$ stratégie sur $A \vdash C$

Pour ce faire, nous allons d'abord parler d'interactions,
il s'agit des suites finies d'événements de $A\otimes B \otimes C$ qui
respecte les conditions de partie sur $A\vdash B,B\vdash C,C\vdash A$.

\begin{answer}{Définition : interaction}

Une interaction sur $A, B, C$ est donnée par une séquence
$u \in (A \otimes B \otimes C)^*$ telle que
\begin{itemize}
\item $u \upharpoonright (A, B) \in \mathcal{P}(A \vdash B)$
\item $u \upharpoonright (B, C) \in \mathcal{P}(B \vdash C)$
\item $u \upharpoonright (A, C) \in \mathcal{P}(A \vdash C)$
\end{itemize}
\end{answer}

(Pour $u = u_1 u_2 \dots u_n, u\upharpoonright (A,B) := (u_i)_{i\in I}
$ où $I = \{i / u_i \text{ événement de }A\text{ ou }B\}$)
\\

On note $I(A,B,C)$ l'ensemble des interactions sur $A,B,C$.
\\

Nous allons ensuite considérer l'ensemble des interactions
qui respectent les stratégies $\sigma$ et $\tau$.

\begin{answer}{Définition : Interactions de stratégies parallèles}
$\sigma || \tau = \{u \in I(A,B,C) \mid u \upharpoonright A,B \in \sigma \text{ et }u \upharpoonright B,C \in \tau\}$
\end{answer}

Enfin nous définissons la stratégie composée.

\begin{answer}{Définition : Composition de stratégies}
$\sigma \circ \tau = \{u \upharpoonright A,C \mid u \in \sigma || \tau\}$
\end{answer}

Expliquons cette définition : nous prouvons dans l'annexe que
pour toute partie
$s$, il existe une unique interaction $u \in \sigma || \tau$ telle que
$u\upharpoonright (A,C) = s$, $u$ sera appelé témoin de $s$.
Alors on voit que $\sigma \circ \tau$ est l'ensemble des parties sur $A\vdash C$
dont le témoin respecte $\sigma$ et $\tau$, c'est à dire que morallement,
on peut avoir de manière intriquée, les parties $A\vdash B, B\vdash C, A\vdash C$
en jouant $\sigma$ et $\tau$.

\section{Définitions équivalentes manipulables en Coq}

Pour faire du Coq nous allons préférer des définitions inductives aux
définitions ensemblistes. Alors, nous allons donner des définitions inductives
aux parties, aux interactions, aux relations entre parties (préfixe, cohérente), etc...
Chaque symbole \faCode  est un hyperlien vers la définition correspondante dans la
documentation HTML autogénérée des fichiers Coq.

Les définitions suivantes sont inchangées dans notre code Coq, on ne cherche pas
à en trouver une caractérisation inductive plus manipulable.

\begin{itemize}
\item Structures d'événements \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.jeu.html\#EventStructure}{\faCode}
\item Configuration \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.jeu.html\#configuration}{\faCode}
\item Justification \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.jeu.html\#justifies}{\faCode}
\item Jeu \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.jeu.html\#Game}{\faCode}
\item Jeu tenseur \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.jeu.html\#Game_tenseur}{\faCode}
\item les interactions de stratégies parallèles \href{https://ulysse\_durand.gitlab.io/stagel3/Codecoq.theories.compose.html\#parallele_stratOO}{\faCode}
\end{itemize}

Il a fallu tout de même prouver qu'une structure d'événement tenseur est une
structure d'événements (plus compliqué) et qu'un jeu dual est bien un jeu.


\subsection{Définition d'une partie}

Une partie étant un mot, nous avons l'habitude pour définir un mot de manière
inductive d'empiler les lettres à la fin du mot. Ici nous allons le faire au
début, ainsi,  retirer le premier coup d'une partie définit toujours une
partie mais dans un autre jeu où on enlève cet événement et ses conflits.

Voyons comment se construit une partie dont le premier coup est un événement
$a$ et la suite des coups est $s$
(notons que $a$ est nécessairement minimal pour la relation de causalité car
on le joue en premier il ne doit dépendre d'aucun événement).
Il faut que $s$ soit une partie sur le jeu sans $a$ et ses conflits, alors on
peut construire une partie $as$ si les polarités sont bien alternantes.

Un tel jeu sans l'événement $a$ et les événements en confilt sera appelé
jeu résiduel en $a$.

\begin{answer}{Definition du jeu résiduel}
Soit $A$ un jeu, $a \in |A|$ minimal pour $\leq_A$, on définit
$A_{\backslash a}$ le jeu résiduel de $A$ en $a$.

\begin{itemize}
\item $|A{\backslash a}| = |A| \backslash \{x \in |A| \mid x \#_A a \text{ ou } x=a\}$
\item $\leq_{A{\backslash a}} = \leq_A|_{|A{\backslash a}|^2}$

(même relation mais restreinte à son nouvel ensemble de définition)

\item $\#_{A{\backslash a}} = \#_A|_{|A{\backslash a}|^2}$

(idem)

\item $\text{pol}_{A{\backslash a}} = \text{pol}_A|_{|A{\backslash a}|}$


(idem)


\end{itemize}

\end{answer}

Un jeu résiduel est un jeu, c'est montré en Coq \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.residu.html#resES}{\faCode}.
\\

Pour faire respecter cette alternance entre coups positifs et négatifs dans
une partie, nous allons faire une définition inductive mutuelle.

Pour construire une partie commençant par un coup positif (resp. négatif)
il faut ajouter un coup négatif (resp. positif) devant une partie
qui commence par un coup positif (resp. négatif).

Nous ne considérerons que les parties finissant par un coup positif,
pour cela la partie vide, seul cas de base, sera considérée comme faisant
partie des parties commençant par un coup négatif.

\begin{answer}{Definition Coq : Les parties \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.strategy.html\#O\_play}{\faCode}}
Les parties commençant par un coup négatif sont les OPlay
et celles commençant par un coup positif sont les PPlay.

$\text{OPlay} \qquad \text{PPlay}$

$\frac{}{\epsilon : \text{OPlay}_G}\epsilon_0$

$\frac{\text{min}(a^-,G) \qquad s : \text{PPlay}_{G\backslash a}}
{a^-s : \text{OPlay}_G}\text{OPlay}_i$

$\frac{\text{min}(a^+,G) \qquad s : \text{OPlay}_{G\backslash a}}
{a^+s : \text{PPlay}_G}\text{PPlay}_i$
\end{answer}

Dans notre code Coq, nous définissons aussi directement les parties sur un jeu
$A\vdash B$ pour faciliter les manipulations ultérieures. On aura alors les types
inductifs $\text{OPlay}_2$ et les $\text{PPlay}_2$ \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.jeuthese.html\#O\_play2}{\faCode}.


\subsection{Définition de stratégies}



Maintenant passons aux stratégies, une stratégie est donc un ensemble de
parties qui est clos par préfixe pair et déterministe.

On définit l'ordre préfixe sur les parties commençant
par un coup négatif et l'ordre préfixe sur les parties commençant par un coup
positif.

\begin{answer}{Définition Coq : Ordre préfixe sur les parties \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.strategy.html\#prefixO}{\faCode}}
$\sqsubseteq^- \subset \text{OPlay}^2 \qquad \sqsubseteq^+ \subset \text{PPlay}^2$

$\frac{s : \text{OPlay}}{\epsilon \sqsubseteq^- s} \sqsubseteq^-_\epsilon$

$\frac{s : \text{PPlay} \qquad s' : \text{PPlay} \qquad s \sqsubseteq^+ s'}
{a^-s \sqsubseteq^- a^-s'} \sqsubset^-_i$

$\frac{s : \text{OPlay} \qquad s' : \text{OPlay} \qquad s \sqsubseteq^- s'}
{a^+s \sqsubseteq^+ a^+s'} \sqsubset^+_i$
\end{answer}

On vient aussi à adapter cette définition sur $\text{OPlay}_2^2$ pour l'ordre
préfixe sur les parties sur un jeu $A\vdash B$ \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.jeuthese.html\#prefixO2}{\faCode}.\\


Il nous reste le déterminisme. Pour ce faire, nous allons le caractériser par
une relation binaire que toutes deux parties d'une stratégie doivent satisfaire.

C'est la relation de cohérence, elle est définie ci-après, et la preuve (pas
en Coq) de l'équivalence avec le déterminisme est donnée en annexe,
c'est la preuve 2.

\begin{answer}{Définition Coq : Cohérence entre deux parties \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.strategy.html\#coherentO}{\faCode}}
$\text{Coh}^- \subset \text{OPlay}^2 \qquad \text{Coh}^+ \subset \text{PPlay}^2$

$\frac{s:\text{Oplay}}{\text{Coh}^-(\epsilon,s)} \text{Coh}^-_{\epsilon,l} \qquad
 \frac{s:\text{OPlay}}{\text{Coh}^-(s,\epsilon)} \text{Coh}^-_{\epsilon,r}$

$\frac{a^- \neq a'^- \qquad s:\text{PPlay} \qquad s':\text{PPlay}}
{\text{Coh}^-(a^-s, a'^- s')} \text{Coh}^-_{\neq i}$

$\frac{s : \text{OPlay} \qquad s':\text{OPlay} \qquad \text{Coh}^-(s,s')}
{\text{Coh}^+(a^+s, a^+s')} \text{Coh}^+_i$

$\frac{s : \text{PPlay} \qquad s':\text{PPlay} \qquad \text{Coh}^+(s,s')}
{\text{Coh}^-(a^-s, a^-s')} \text{Coh}^-_i$

\end{answer}

On adapte aussi cette définition pour la cohérence entre les $\text{OPlay}_2$ et les
$\text{PPlay}_2$ \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.jeuthese.html\#coherentO2}{\faCode}.

Cela nous permet bien de définir formellement les stratégies
\href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.strategy.html#strategy}{\faCode}
ainsi que les stratégies sur les jeux $A\vdash B$
\href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.jeuthese.html#strategy2O}{\faCode}.

\subsection{Composition de stratégies}

Pour composer les stratégies nous allons aussi définir les interactions
de manière inductive.
Etudions de plus près quels "coups" on peut rajouter à gauche d'une interaction
tout en gardant une interaction.


\begin{answernopage}{Diagramme de polarité}

À une interaction $u \in I(A,B,C)$, on peut associer un état
$e_1 e_2 e_3 \in \{O,P\}^3$ tel que
\begin{itemize}
\item $e_1 = O$ si dans la partie $A\vdash B, u\upharpoonright A,B $ est une partie où c'est au tour de l'opposant
\item $e_2 = O$ si dans la partie $B\vdash C, u\upharpoonright B,C $ est une partie où c'est au tour de l'opposant
\item $e_3 = O$ si dans la partie $A\vdash C, u\upharpoonright A,C $ est une partie où c'est au tour de l'opposant
\end{itemize}

On aboutit alors au diagramme suivant :

\tikzfig{diagpol}

où on passe d'un état à un autre en ajoutant un événement à droite de
notre interaction, $a^+$ désigne n'importe quel événement de $A$
de polarité $+$.

On a aussi le diagramme lorsqu'on ajoute un événement à gauche de notre
interaction, ce que l'on va faire. Alors c'est ce second diagramme qui
nous sera utile pour définir les interactions inductivement.

\tikzfig{diagpolbis}


\end{answernopage}

En s'aidant de ce diagramme, on peut faire une définition inductive mutuelle
pour nos interactions, entre les interactions dans les états OOO, POP et OPP.

\begin{answer}{Définition Coq : Interactions \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.interactions.html\#OOO\_int}{\faCode}}
OOOInt \qquad OPPInt \qquad POPInt


$\frac{}{\epsilon : OOOInt} OOO_{\epsilon}$

$\frac{s : POPInt \qquad a^+ : A}{a^+ s : OOOInt} aPOP \qquad
 \frac{s : OPPInt \qquad c^- : C}{c^- s : OOOInt} cOPP$

$\frac{s : OPPInt \qquad b^+ : B}{b^+ s : POPInt} bOPP \qquad
 \frac{s : OOOInt \qquad a^- : A}{a^- s : POPInt} aOOO$

$\frac{s : OOOInt \qquad c^+ : C}{c^+ s : OPPInt} cOOO \qquad
 \frac{s : POPInt \qquad b^- : B}{b^- s : OPPInt} bPOP$
\end{answer}

Après avoir adapté les définitions de parties aux jeux $A\vdash B$
nous définissons aussi les restrictions d'interactions sur $A, B$ et $C$
aux parties $A\vdash B, B\vdash C, A\vdash C$ \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.interactions.html\#restriction\_lm\_OOO}{\faCode}.

Cette déinition inductive d'interactions est bien équivalente à celle vue
plus tôt car on arrive à construire des fonction de restriction d'interaction
qui ont le bon type ($\text{OOOInt} \rightarrow \text{OPlay}_2$ par exemple).

On définit aussi naturellement la relation de préfixe sur les interactions \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.interactions.html\#prefixOOO}{\faCode}.

Pour faire nos preuves sur les structures inductives de parties,
on définit les stratégies résiduelles.

\begin{answer}{Stratégie résiduelle \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.residustrat.html\#residu\_stratO\_l}{\faCode}}
Une stratégie residuelle $\sigma_{\backslash a}$ est une stratégie sur le jeu
$A\backslash a$ telle que

$s \in \sigma_{\backslash a} \iff as \in \sigma$
\end{answer}
On se retouve à définir alors deux définitions de stratégie résiduelle suivant si
l'événement exclu est dans le jeu de gauche ou de droite.

Pour faire la preuve du déterminisme d'une stratégie composée, il a fallu aussi
définie la relation de cohérence sur les interactions, de manière comparable à
la manière dont ça a été défini sur les parties. \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.interactions.html\#CohOOO}{\faCode}

Enfin, il nous a fallu entre autres prouver que l'ensemble des interactions sur les
stratégies parallèles est stable par préfixe. \href{https://ulysse_durand.gitlab.io/stagel3/Codecoq.theories.compose.html\#prefixe\_donc\_paralleleOO}{\faCode}

\newpage
\section{Conclusion}

Le travail que j'ai fourni pendant ce stage tend à laisser penser que le
formalisme de Pierre Clairambault semble bien adéquat pour un formalisme des
jeux et stratégies en assistant de preuves.
La reformulation inductive d'un bon nombre d'éléments est au final possible,
chose intuitée par mes maitres de stage mais réalisée ici.

Il manque surtout à tout ce travail un bon nombre de "sanity check",
c'est à dire des preuves que les définitions inductives
que l'on a construit correspondent bien aux définitions ensemblistes
(Quelques uns de ces sanitycheck sont dans le fichier sanitycheck.v).
\footnote{Il y a un admitted dans le code, ce n'est qu'à cause d'un manque de
temps, la preuve est accessible avec le principe d'induction sur la structure
inductive mutuelle}

Il faut aussi rappeler que je ne suis pas parti de rien pour ce travail,
Etienne Miquey avait déjà commencé une super esquisse et avancé les idées,
son travail que j'ai poursuivi est disponible dans le
fichier Games\_ind.v.
\\

Dans de ce stage, j'ai pu explorer certains outils de Coq qui m'étaient
inconnus au par avant.
Lors des preuves il a fallu à un moment utiliser les Schemes de Coq,
qui permettent de générer automatiquement les principes d'induction sur les
structures mutuellement inductives (très utilisées).

Aussi, beaucoup de $\Sigma$-Types sont utilisés par Coq, mes encadrants de
stage m'ont alors conseillé de lire le chapitre 1 du livre
\href{https://homotopytypetheory.org/book/}{HOTT} pour
comprendre ce dont il s'agit.

Mes encadrants en ont aussi profité pour un peu mieux me montrer comment utiliser
la correspondance de Curry Howard dans Coq.
\\

Ce travail a pour but d'être poursuivi par la suite. Il reste de nombreux sanity
check à faire et ensuite il faudra prouver que la composition de stratégies est
associative, définir Copycat et montrer que c'est le neutre de la composition,
et même pourquoi pas aller jusqu'à montrer que la catégorie ainsi construite est
une catégorie cartésienne fermée.

\newpage
\section{Annexes}
\newpage
\begin{answer}{Preuve 1 :}

\begin{answer}{Lemme d'unicité du témoins :}
Si $s \in \sigma \circ \tau$ est de longueur paire,
$\exists ! u \in \sigma||\tau $ tel que $ s = u \upharpoonright A,C$

on l'appelle le témoins de s.

\end{answer}

\begin{answer}{Preuve du Lemme :}
Existence :

Par définition de $\sigma \circ \tau$
\\

Unicité :

Soit $u,v \in \sigma || \tau$ tels que
$u \upharpoonright A,C = s$ et $v \upharpoonright A, C = s$.

Soit $\omega$ leur plus long préfixe commun
\begin{alignat*}{3}
&u = &\omega &&l_1 &u'\\
&v = &\omega &&l_2 &v'
\end{alignat*}
\begin{itemize}
\item Si $l_1$ et  $l_2 \in B$.

Comme $u$ et $v$ sont alternants pour $\text{pol}_B$,
on a $\text{pol}_B(b_1) = \text{pol}_B(b_2)$

Supposons que ça vaut $+$.

Par déterminisme de $\tau$, comme
$(w\upharpoonright B,C)b_1^+ \in \tau$ et
$(w\upharpoonright B,C)b_2^+ \in \tau$,
on a $b_1$ = $b_2$.

\item Sinon

C'est impossible compte tenu du diagramme de polarité

\end{itemize}

\end{answer}
\end{answer}

\newpage

\begin{answernopage}{Preuve que cohérente et clos par préfixe équivaut à déterministe et clos par préfixe.}
On dit que $\sigma \subset \mathcal{P}(A)$ est cohérente si
$\forall s, s' \in \sigma$ de longueur paire, $ \text{Coh}^-(s,s')$

Alors si $\sigma$ est cohérente

Montrons $\forall s, sa_1^+ \in \sigma$ et $s a^+ \in \sigma \implies a_1 = a_2$
par induction sur $|s|$, (IH) est notre hypothèse d'induction.

\begin{itemize}
\item $|s| = 1$

Alors comme $\sigma$ cohérente, on a $\text{Coh}^-(s_1^-a_1^+,s_1^-a_2^+)$.
On analyse comment ça a pu être construit avec les règles d'inférence,
\begin{itemize}
\item via $\text{Coh}^-_{\neq i}$, on aurait $s_1 \neq s_1$, impossible.
\item via $\text{Coh}^-_{\epsilon,l}$, on aurait $s_1a_1^+ = \epsilon$, impossible.
\item via $\text{Coh}^-_{\epsilon,r}$, on aurait $s_1a_2^+ = \epsilon$, impossible.
\item on a alors forcément via $\text{Coh}^-_i$, $\text{Coh}^+(a_1^+,a_2^+)$
et de même en regardant quelles règles d'inférences auraient pu le construire,
on obtient $a_1 = a_2$.

\end{itemize}
\item $|s| > 1$

Alors comme $\sigma$ cohérente, on a $\text{Coh}^-(s_1^-s_2^+s'a_1+,s_1^-s_2^+s'a_2^+)$

Comme pour le cas $|s|=1$, on regarde les règles d'inférence le construisant,
le seul cas possible étant $\text{Coh}^+(s_2^+s'a_1^+, s_2^+s'a_2^+)$,
en faisant encore une fois l'analyse
des règles d'inférence construisant $\text{Coh}^+$,
on déduit $\text{Coh}^-(s'a_1^+, s'a_2^+)$.

En appliquant l'hypothèse (IH), sur $\text{Coh}^-(s'a_1^+,s'a_2^+)$, on
obtient $a_1 = a_2$

\end{itemize}

Montrons maintenant la réciproque

Si $\sigma$ est déterministe et clos par préfixe

On montre $\forall s, s' \in \sigma, \text{Coh}^-_G(s,s')$ par induction sur
$|s|+|s'|$.

Si $s=\epsilon$ ou $s'=\epsilon$ c'est bon grâce à
$\text{Coh}^-_{\epsilon, l}$ et $\text{Coh}^-_{\epsilon, r}$, sinon
$s = s_1 s_2 s_\geq$ et $s' = s'_1 s'_2 s'_\geq$
\begin{itemize}
\item Si $s_1 = s'_1$

Comme $\sigma$ est clos par préfixe, on a $s_1 s_2, s'_1 s'_2 \in \sigma$.
Par déterminisme, on a alors $s_2 = s'_2$.

Comme on a $s_\geq, s'_\geq \in \sigma |_{G\backslash s_1,s_2}$, par IH,
on a $\text{Coh}^-(s_\geq, s'_\geq)$

Donc on a $\text{Coh}^+(s_2 s_\geq, s'_2 s'_\geq)$ et finalement,
$\text{Coh}^-(s_1 s_2 s_\geq, s'_1 s'_2 s'_\geq)$ grâce à
$\text{Coh}_i^+$ et $\text{Coh}_i^-$.


\item Si $s_1 \neq s'_1$

On a directement par $\text{Coh}_{\neq i}^-, \text{Coh}^-(s_1 s_2 s_\geq, s'_1 s'_2 s_\geq)$.

\end{itemize}
\end{answernopage}

\newpage
\section{Contexte institutionnel et social}

Je me suis retrouvé dans le campus de Luminy à Marseille avec deux équipes entre
le LIS (équipe LIRICA) et l'I2M (équipe LDP), soit un laboratoire de mathématiques
et un d'informatique.

Le lien entre les deux équipes était très fort, tous travaillaient sensiblement
sur les mêmes sujets, faisaient leur séminaires ensembles et mangeaient ensembles.
L'ambiance était très conviviale (foot, escalade, bar et dîner entre
chercheur.euses),
surtout avec le cadre, c'est à dire un campus au milieu des calanques,
dont j'ai aussi pu profiter.

J'ai pu assister à des séminaires bien intéressants mais dur à suivre, ils
étaient à une fréquence hébdomadaire et couvraient des sujets autour
de la correspondance de Curry Howard, le lambda calcul, et la logique linéaire
qui est apparement la spécialité locale avec les travaux du célèbre
Jean-Yves Girard, ancien chercheur du laboratoire de l'I2M.

J'ai pu constater que tous.tes devaient en plus de leur recherche accomplir
d'autres tâches qui les faisaient se déplacer, Pierre Clairambault a par
exmemple dû faire jury de TIPE et Etienne Miquey présenter un papier à une
conférence à Rome. Le métier de chercheur me semble requiérir une forte
autodiscipline et organisation.

\printbibliography
\end{document}
